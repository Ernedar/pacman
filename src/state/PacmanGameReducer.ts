import {
  InhabitantNames,
  GameStateType,
  GhostStates,
  PacManStates,
  TileType,
  GameActionType,
} from '../utils/enums';
import { populatePowerPoints, startPositionHandler } from '../utils/handlers';
import { gameStateInterface } from '../utils/interfaces';
import { GameActions } from '../utils/types';

export default function GameReducer(
  state: gameStateInterface,
  action: GameActions
): gameStateInterface {
  switch (action.type) {
    case GameActionType.InitiateGame:
      return {
        ...state,
        game: {
          ...state.game,
          gameState: GameStateType.Notstarted,
        },
        entity: {
          ...state.entity,
          pacman: {
            ...state.entity.pacman,
            entityStartPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Pacman
            ),
            entityCurrentPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Pacman
            ),
          },
          clyde: {
            ...state.entity.clyde,
            entityStartPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Clyde
            ),
            entityCurrentPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Clyde
            ),
          },
          pinky: {
            ...state.entity.pinky,
            entityStartPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Pinky
            ),
            entityCurrentPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Pinky
            ),
          },
          blinky: {
            ...state.entity.blinky,
            entityStartPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Blinky
            ),
            entityCurrentPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Blinky
            ),
          },
          inky: {
            ...state.entity.inky,
            entityStartPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Inky
            ),
            entityCurrentPosition: startPositionHandler(
              action.payload.mazeArray,
              InhabitantNames.Inky
            ),
          },
        },
        points: populatePowerPoints(action.payload.mazeArray, TileType.Point),
        powers: populatePowerPoints(action.payload.mazeArray, TileType.Power),
      };
    case GameActionType.GameLoaded:
      console.log('Game is loaded.');
      return {
        ...state,
        game: {
          ...state.game,
          gameLoaded: true,
        },
      };
    case GameActionType.ChangeGameStatus:
      if (action.payload.gameStatus === GameStateType.Notstarted) {
        /* RESET helper function needed, this is a mess, not even points and powers present. */
        return {
          ...state,
          game: {
            ...state.game,
            gameState: GameStateType.Notstarted,
            gameScore: 0,
          },
          entity: {
            ...state.entity,
            pacman: {
              ...state.entity.pacman,
              entityState: PacManStates.Idle,
              entityCurrentPosition: state.entity.pacman.entityStartPosition,
              entityCurrentDirection: [0, 0],
              entityActionCounter: 0,
              entityDeltaCounter: 0,
            },
            clyde: {
              ...state.entity.clyde,
              entityState: GhostStates.Idle,
              entityCurrentPosition: state.entity.clyde.entityStartPosition,
              entityCurrentDirection: [0, 0],
              entityActionCounter: 0,
              entityDeltaCounter: 0,
            },
            pinky: {
              ...state.entity.pinky,
              entityState: GhostStates.Idle,
              entityCurrentPosition: state.entity.pinky.entityStartPosition,
              entityCurrentDirection: [0, 0],
              entityActionCounter: 0,
              entityDeltaCounter: 0,
            },
            blinky: {
              ...state.entity.blinky,
              entityState: GhostStates.Idle,
              entityCurrentPosition: state.entity.blinky.entityStartPosition,
              entityCurrentDirection: [0, 0],
              entityActionCounter: 0,
              entityDeltaCounter: 0,
            },
            inky: {
              ...state.entity.inky,
              entityState: GhostStates.Idle,
              entityCurrentPosition: state.entity.inky.entityStartPosition,
              entityCurrentDirection: [0, 0],
              entityActionCounter: 0,
              entityDeltaCounter: 0,
            },
          },
        };
      } else if (action.payload.gameStatus in GameStateType) {
        return {
          ...state,
          game: {
            ...state.game,
            gameState: action.payload.gameStatus,
          },
        };
      } else {
        return state;
      }
    case GameActionType.ChangeEntityDirection:
      if (action.payload.entity in InhabitantNames) {
        return {
          ...state,
          entity: {
            ...state.entity,
            [action.payload.entity]: {
              ...state.entity[action.payload.entity],
              entityCurrentDirection: action.payload.direction,
            },
          },
        };
      } else {
        return state;
      }
    case GameActionType.UpdateEntityActionCounter:
      if (action.payload.entity in InhabitantNames) {
        return {
          ...state,
          entity: {
            ...state.entity,
            [action.payload.entity]: {
              ...state.entity[action.payload.entity],
              entityDeltaCounter: action.payload.entityDeltaCounter,
              entityActionCounter:
                state.entity[action.payload.entity].entityActionCounter +
                action.payload.entityActionCounter,
            },
          },
        };
      } else {
        return state;
      }
    case GameActionType.UpdateEntityCurrentPosition:
      if (action.payload.entity in InhabitantNames) {
        return {
          ...state,
          entity: {
            ...state.entity,
            [action.payload.entity]: {
              ...state.entity[action.payload.entity],
              entityCurrentPosition: action.payload.newPosition,
            },
          },
        };
      } else {
        return state;
      }
    default:
      return state;
  }
}
