import { DesignerActionType, DesignerTileType } from '../utils/enums';
import { DesignerActions } from '../utils/types';
import { designerStateInterface } from '../utils/interfaces';
import {
  changeMazeDesignerTile,
  addTileToMaze,
  addNewRowToMaze,
  removeTileFromMaze,
  clearMazeRow,
  clearMazeColumn,
} from '../utils/handlers';

export default function MazeDesignerReducer(
  state: designerStateInterface,
  action: DesignerActions
): designerStateInterface {
  switch (action.type) {
    case DesignerActionType.SelectTileAction:
      return {
        ...state,
        selectedTileType: action.payload.tileType,
        selectedTileNumber: action.payload.tileNumber,
        selectedTileClass: action.payload.tileClass,
      };
    case DesignerActionType.ClearSelectedTileAction:
      return {
        ...state,
        selectedTileType: DesignerTileType.Path,
        selectedTileNumber: 0,
        selectedTileClass: '',
      };
    case DesignerActionType.AddTileToMazeAction:
      return {
        ...state,
        designedMaze: addTileToMaze(
          state.designedMaze,
          action.payload.xCoord,
          action.payload.tileNumber
        ),
      };
    case DesignerActionType.ChangeMazeTileAction:
      return {
        ...state,
        designedMaze: changeMazeDesignerTile(
          state.designedMaze,
          action.payload.tileNumber,
          action.payload.xCoord,
          action.payload.yCoord
        ),
      };
    case DesignerActionType.ClearMazeTileAction:
      return {
        ...state,
        designedMaze: changeMazeDesignerTile(
          state.designedMaze,
          0,
          action.payload.xCoord,
          action.payload.yCoord
        ),
      };
    case DesignerActionType.RemoveTileFromMazeAction:
      return {
        ...state,
        designedMaze: removeTileFromMaze(
          state.designedMaze,
          action.payload.xCoord
        ),
      };
    case DesignerActionType.AddRowOfTilesAction:
      return {
        ...state,
        designedMaze: addNewRowToMaze(
          state.designedMaze,
          action.payload.tileNumber
        ),
      };
    case DesignerActionType.ClearRowOfTilesAction:
      return {
        ...state,
        designedMaze: clearMazeRow(state.designedMaze, action.payload.xCoord),
      };
    case DesignerActionType.ClearColOfTilesAction:
      return {
        ...state,
        designedMaze: clearMazeColumn(
          state.designedMaze,
          action.payload.yCoord
        ),
      };
    case DesignerActionType.SetMazeNameAction:
      return {
        ...state,
        designedMazeName: action.payload.mazeName,
      };
    case DesignerActionType.SetMazeLinkTagAction:
      return {
        ...state,
        designedMazeLinkTag: action.payload.mazeLinkTag,
      };
    case DesignerActionType.SaveMazeAction:
      return {
        ...state,
      };
    case DesignerActionType.RefreshStateRenderAction:
      return {
        ...state,
        designedMaze: state.designedMaze,
      };
    case DesignerActionType.ClearDesignerAction:
      return {
        ...state,
        designedMazeID: 0,
        designedMazeName: '',
        designedMazeLinkTag: '',
        designedMaze: [[]],
      };
    default:
      return state;
  }
}
