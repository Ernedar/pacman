import React, { FC, createContext, useContext, useReducer, Dispatch } from "react";
import { DesignerActions } from "../utils/types";
import { designerStateInterface } from "../utils/interfaces";
import MazeDesignerInitState from "./initialDesignerState";
import MazeDesignerReducer from "./mazeDesignerReducer";

const DesignerStateContext = createContext<designerStateInterface>(MazeDesignerInitState);
const DesignerDispatchContext = createContext<Dispatch<DesignerActions>>(() => undefined);

export const useDesignerState = () => {
	const stateContext = useContext(DesignerStateContext);
	if(stateContext === undefined) {
		throw new Error(
		  "useDesignerState must be use withing a DesignerStateContext. For more information look in to mazeDesignerContext.tsx file."
		);
	}
	return stateContext;
};

export const useDesignerDispatch = () => {
	const dispatchContext = useContext(DesignerDispatchContext);
	if(dispatchContext === undefined) {
		throw new Error(
		  "useDesignerDispatch must be use withing a DesignerDispatchContext. For more information look in to mazeDesignerContext.tsx file."
		);
	}
	return dispatchContext;
};

type designerContextProps = {
	children: React.ReactNode;
}

export const DesignerContextProvider: FC<designerContextProps> = ({children}) => {
	const [state, dispatch] = useReducer(MazeDesignerReducer, MazeDesignerInitState);
	return (
		<DesignerStateContext.Provider value={state}>
			<DesignerDispatchContext.Provider value={dispatch}>
				{children}
			</DesignerDispatchContext.Provider>
		</DesignerStateContext.Provider>
	);
};