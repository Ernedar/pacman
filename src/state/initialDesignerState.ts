import { DesignerTileType } from '../utils/enums';
import { designerStateInterface } from '../utils/interfaces';

const MazeDesignerInitState: designerStateInterface = {
  selectedTileType: DesignerTileType.Path,
  selectedTileNumber: 0,
  selectedTileClass: '',
  designedMazeID: 0,
  designedMazeName: '',
  designedMazeLinkTag: '',
  designedMaze: [[]],
};

export default MazeDesignerInitState;
