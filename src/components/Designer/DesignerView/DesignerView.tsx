import React, { FC } from "react";

import "./DesignerView.css";

import { clearDesignerAction, DESIGNER_ACTIONS, setMazeLinkTagAction, setMazeNameAction } from "../../../utils/actions";
import { designerProps } from "../../../utils/types";
import { designerCellActions } from "../../../utils/enums";

import DesignerViewButton from "../DesignerViewButton";
import AddClearButton from "../AddClearButton";
import classNames from "classnames";
import { useDesignerDispatch, useDesignerState } from "../../../state/mazeDesignerContext";

const DesignerView: FC<designerProps> = () => {
  const state = useDesignerState();
  const dispatch = useDesignerDispatch();
  if (state.designedMaze) {
    return (
      <div className="designer-view-wrapper">
        <div className="designer-view-action-bar">
          <div className="action-bar-left">
            <label
              className="input-label input-md right-adj"
              htmlFor="mazeName"
            >
              Maze Name:
            </label>
            <input
              className="form-input input-md left-adj"
              type="text"
              name="mazeName"
              id="mazeName"
              onChange={(e) => {
                dispatch(setMazeNameAction(e.target.value));
              }}
            />
            <label className="input-label input-md right-adj" htmlFor="mazeTag">
              Maze linkTag:
            </label>
            <input
              className="form-input input-md left-adj"
              type="text"
              name="mazeTag"
              id="mazeTag"
              onChange={(e) => {
                dispatch(setMazeLinkTagAction(e.target.value));
              }}
            />
          </div>
          <div className="action-bar-right">
            <button
              className="btn btn-success btn-md"
              onClick={() => {
                state.designedMaze &&
                  navigator.clipboard.writeText(
                    state.designedMaze[0].length.toString() +
                      "," +
                      state.designedMaze.toString()
                  );
              }}
            >
              Save Maze
            </button>
            <button
              className="btn btn-danger btn-md"
              onClick={() =>
                dispatch(clearDesignerAction())
              }
            >
              Clear Maze
            </button>
          </div>
        </div>
        <table cellSpacing="0">
          <thead>
            <tr>
              <th>
                <div className="legend-cell empty"></div>
              </th>
              <th>
                <div className="legend-cell empty"></div>
              </th>
              {state.designedMaze[0].map((cell, i) => (
                <th key={"mazeColPre" + i}>
                  <div className="action-cell">
                    <AddClearButton
                      cellAction={designerCellActions.ClearCol}
                      colNumber={i}
                    />
                  </div>
                </th>
              ))}
            </tr>
            <tr>
              <th>
                <div className="legend-cell empty"></div>
              </th>
              <th>
                <div className="legend-cell empty"></div>
              </th>
              {state.designedMaze[0].map((cell, i) => (
                <th key={"mazeCol" + i}>
                  <div className="legend-cell horizontal">
                    <p>{i}</p>
                  </div>
                </th>
              ))}
              <th>
                <div className="legend-cell horizontal">
                  <p>{state.designedMaze[0].length}</p>
                </div>
              </th>
            </tr>
          </thead>
          <tbody className="design-maze">
            {state.designedMaze.map((mazeLine, x) => (
              <tr key={"mazeLine" + x}>
                <td
                  className={classNames("legend-cell-outside", {
                    visible:
                      state.designedMaze &&
                      state.designedMaze[x].length > 0
                  })}
                >
                  <div className="action-cell">
                    <AddClearButton
                      cellAction={designerCellActions.ClearRow}
                      rowNumber={x}
                    />
                  </div>
                </td>
                <td>
                  <div className="legend-cell vertical">
                    <p>{x}</p>
                  </div>
                </td>
                {mazeLine.map((mazeCellID, y) => (
                  <td key={"mazeCell-" + x + "-" + y}>
                    <DesignerViewButton
                      x={x}
                      y={y}
                      mazeCell={mazeCellID}
                    />
                  </td>
                ))}
                <td>
                  <div className="action-cell">
                    <AddClearButton
                      cellAction={designerCellActions.AddTile}
                      rowNumber={x}
                    />
                  </div>
                </td>
                <td
                  className={classNames("legend-cell-inside", {
                    visible:
                      state.designedMaze &&
                      state.designedMaze[x].length > 0
                  })}
                >
                  <div className="action-cell">
                    <AddClearButton
                      cellAction={designerCellActions.RemoveTile}
                      rowNumber={x}
                    />
                  </div>
                </td>
              </tr>
            ))}
            <tr
              className={classNames("legend-row-link", {
                visible: state.designedMaze[0].length > 0
              })}
            >
              <td>
                <div className="legend-cell empty"></div>
              </td>
              <td>
                <div className="legend-cell vertical">
                  <p>{state.designedMaze.length}</p>
                </div>
              </td>
              <td>
                <div className="action-cell">
                  <AddClearButton
                    cellAction={designerCellActions.AddRow}
                  />
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  } else {
    return null;
  }
};

export default DesignerView;
