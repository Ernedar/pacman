import React, { FC, useState } from "react";
import classNames from "classnames";

import "./DesignerLegend.css";

import LegendButton from "../LegendButton";

import {
  MazeWall,
  GhostHome,
  PortalDirection,
  DesignerTileType
} from "../../../utils/enums";

import { designerStateInterface } from "../../../utils/interfaces";
import { useDesignerDispatch, useDesignerState } from "../../../state/mazeDesignerContext";
import { clearSelectedTileAction } from "../../../utils/actions";

const numberRegexPattern = /\d+/g;

function renderBuildingBlocks(
  enumImport: Object,
  type: DesignerTileType,
  designerState: designerStateInterface
) {
  const array = Object.keys(enumImport);

  const buildingBlocks = array.map((key) => {
    const tileNumberFromKey = key.match(numberRegexPattern) || [];

    const tileFinalNumber = parseInt(tileNumberFromKey[0], 0);

    if (designerState) {
      return (
        <LegendButton
          key={"TileButtonKey" + key}
          tileType={type}
          tileNumber={tileFinalNumber}
          tileKey={tileFinalNumber.toString()}
        />
      );
    }
  });

  return buildingBlocks;
}

const DesignerLegend: FC = () => {
  const [selectedTab, setSelectedTab] = useState(0);
  const state = useDesignerState();
  const dispatch = useDesignerDispatch();

  if (state) {
    return (
      <div
        className="designer-legend-wrapper"
        onAuxClick={() =>
          dispatch(clearSelectedTileAction())
        }
      >
        <div className="designer-legend-header">
          <button
            className={classNames("tab-button", { active: selectedTab === 0 })}
            onClick={() => setSelectedTab(0)}
          >
            Walls
          </button>
          <button
            className={classNames("tab-button", { active: selectedTab === 1 })}
            onClick={() => setSelectedTab(1)}
          >
            Portals
          </button>
          <button
            className={classNames("tab-button", { active: selectedTab === 2 })}
            onClick={() => setSelectedTab(2)}
          >
            Ghost Home
          </button>
          <button
            className={classNames("tab-button", { active: selectedTab === 3 })}
            onClick={() => setSelectedTab(3)}
          >
            Entities
          </button>
        </div>
        <div className="designer-legend-body">
          <div
            className={classNames("designer-legend-tab", {
              active: selectedTab === 0
            })}
          >
            {renderBuildingBlocks(
              MazeWall,
              DesignerTileType.Wall,
              state
            )}
          </div>
          <div
            className={classNames("designer-legend-tab", {
              active: selectedTab === 1
            })}
          >
            {renderBuildingBlocks(
              PortalDirection,
              DesignerTileType.Door,
              state
            )}
          </div>
          <div
            className={classNames("designer-legend-tab", {
              active: selectedTab === 2
            })}
          >
            {renderBuildingBlocks(
              GhostHome,
              DesignerTileType.Ghome,
              state
            )}
          </div>
          <div
            className={classNames("designer-legend-tab", {
              active: selectedTab === 3
            })}
          >
            <LegendButton
              tileType={DesignerTileType.Path}
              tileNumber={0}
              tileKey="path"
            />
            <LegendButton
              tileType={DesignerTileType.Pacman}
              tileNumber={1}
              tileKey="pacman"
            />
            <LegendButton
              tileType={DesignerTileType.Clyde}
              tileNumber={21}
              tileKey="ghost"
            />
            <LegendButton
              tileType={DesignerTileType.Inky}
              tileNumber={22}
              tileKey="ghost"
            />
            <LegendButton
              tileType={DesignerTileType.Pinky}
              tileNumber={23}
              tileKey="ghost"
            />
            <LegendButton
              tileType={DesignerTileType.Blinky}
              tileNumber={24}
              tileKey="ghost"
            />
            <LegendButton
              tileType={DesignerTileType.Point}
              tileNumber={31}
              tileKey="point"
            />
            <LegendButton
              tileType={DesignerTileType.Power}
              tileNumber={32}
              tileKey="power"
            />
          </div>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

export default DesignerLegend;
