import React, { FC } from "react";
import classNames from "classnames";

import "./AddClearButton.css";

import { designerCellActions } from "../../../utils/enums";
import { addRowOfTilesAction, addTileToMazeAction, clearColOfTilesAction, clearRowOfTilesAction, removeTileFromMazeAction } from "../../../utils/actions";
import { useDesignerDispatch, useDesignerState } from "../../../state/mazeDesignerContext";

type addClearButtonProps = {
  cellAction: designerCellActions;
  rowNumber?: number;
  colNumber?: number;
};

const AddClearButton: FC<addClearButtonProps> = ({
  cellAction,
  rowNumber,
  colNumber
}) => {
  const state = useDesignerState();
  const dispatch = useDesignerDispatch();

  if (state.designedMaze) {
    if (cellAction === designerCellActions.AddTile && rowNumber) {
      return (
        <button
          className={classNames("add-clear-btn add-btn")}
          onClick={() =>
            dispatch(addTileToMazeAction(state.selectedTileNumber, rowNumber))
          }
        />
      );
    } else if (
      cellAction === designerCellActions.AddRow &&
      state.designedMaze[0].length > 0
    ) {
      return (
        <button
          className={classNames("add-clear-btn add-btn")}
          onClick={() =>
            dispatch(addRowOfTilesAction(state.selectedTileNumber))
          }
        />
      );
    } else if (cellAction === designerCellActions.RemoveTile && rowNumber) {
      return (
        <button
          className={classNames("add-clear-btn rmw-btn")}
          onClick={() =>
            dispatch(removeTileFromMazeAction(rowNumber))
          }
        />
      );
    } else if (cellAction === designerCellActions.ClearRow && rowNumber) {
      return (
        <button
          className={classNames("add-clear-btn clr-btn")}
          onClick={() =>
            dispatch(clearRowOfTilesAction(rowNumber))
          }
        >
          <p>C</p>
        </button>
      );
    } else if (
      cellAction === designerCellActions.ClearCol &&
      state.designedMaze[0].length > 0 && colNumber
    ) {
      return (
        <button
          className={classNames("add-clear-btn clr-btn")}
          onClick={() =>
            dispatch(clearColOfTilesAction(colNumber))
          }
        >
          <p>C</p>
        </button>
      );
    } else {
      return null;
    }
  } else {
    return null;
  }
};

export default AddClearButton;
