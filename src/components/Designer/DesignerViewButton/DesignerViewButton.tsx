import React, { FC } from "react";
import classNames from "classnames";

import {
  DesignerTileType,
  MazeWall,
  GhostHome,
  PortalDirection,
  Inhabitants
} from "../../../utils/enums";
import { changeMazeTileAction, clearMazeTileAction, DESIGNER_ACTIONS } from "../../../utils/actions";

import { CNGroup } from "../../../utils/logo";
import Icon from "../../Icons";

import "./DesignerViewButton.css";
import { useDesignerDispatch, useDesignerState } from "../../../state/mazeDesignerContext";

type designerViewButtonProps = {
  mazeCell: number;
  x: number;
  y: number;
};

const DesignerViewButton: FC<designerViewButtonProps> = ({
  mazeCell,
  x,
  y
}) => {
  const state = useDesignerState();
  const dispatch = useDesignerDispatch();

  if (mazeCell >= 60 && mazeCell <= 69) {
    const enumTileKey = ("P" + mazeCell) as keyof typeof PortalDirection;

    return (
      <button
        className="designed-maze-button"
        onClick={() =>
          dispatch(changeMazeTileAction(state.selectedTileNumber, x, y))
        }
        onAuxClick={() =>
          dispatch(clearMazeTileAction(x, y))
        }
      >
        <Icon
          className={PortalDirection[enumTileKey]}
          iconToLoad={DesignerTileType.Door}
        />
      </button>
    );
  } else if (mazeCell >= 100 && mazeCell < 200) {
    const enumTileKey = ("G" + mazeCell) as keyof typeof GhostHome;

    return (
      <button
        className="designed-maze-button"
        onClick={() =>
          dispatch(changeMazeTileAction(state.selectedTileNumber, x, y))
        }
        onAuxClick={() =>
          dispatch(clearMazeTileAction(x, y))
        }
      >
        <Icon
          className={GhostHome[enumTileKey]}
          iconToLoad={DesignerTileType.Ghome}
        />
      </button>
    );
  } else if (mazeCell >= 200 && mazeCell < 300) {
    const enumTileKey = ("W" + mazeCell) as keyof typeof MazeWall;

    return (
      <button
        className="designed-maze-button"
        onClick={() =>
          dispatch(changeMazeTileAction(state.selectedTileNumber, x, y))
        }
        onAuxClick={() =>
          dispatch(clearMazeTileAction(x, y))
        }
      >
        <Icon
          className={MazeWall[enumTileKey]}
          iconToLoad={DesignerTileType.Wall}
        />
      </button>
    );
  } else if (mazeCell === 31) {
    return (
      <button
        className="designed-maze-button"
        onClick={() =>
          dispatch(changeMazeTileAction(state.selectedTileNumber, x, y))
        }
        onAuxClick={() =>
          dispatch(clearMazeTileAction(x, y))
        }
      >
        <div className="point-wrapper">
          <div className="point" />
        </div>
      </button>
    );
  } else if (mazeCell === 32) {
    return (
      <button
        className="designed-maze-button"
        onClick={() =>
          dispatch(changeMazeTileAction(state.selectedTileNumber, x, y))
        }
        onAuxClick={() =>
          dispatch(clearMazeTileAction(x, y))
        }
      >
        <div className="power">
          <img src={CNGroup} alt="CN Group Logo" />
        </div>
      </button>
    );
  } else if (mazeCell === 1) {
    return (
      <button
        className="designed-maze-button"
        onClick={() =>
          dispatch(changeMazeTileAction(state.selectedTileNumber, x, y))
        }
        onAuxClick={() =>
          dispatch(clearMazeTileAction(x, y))
        }
      >
        <div className="pacman">
          <div className="top-half"></div>
          <div className="bottom-half"></div>
          <div className="eye"></div>
        </div>
      </button>
    );
  } else if (mazeCell >= 21 && mazeCell <= 24) {
    return (
      <button
        className="designed-maze-button"
        onClick={() =>
          dispatch(changeMazeTileAction(state.selectedTileNumber, x, y))
        }
        onAuxClick={() =>
          dispatch(clearMazeTileAction(x, y))
        }
      >
        <div className={classNames("ghost", Inhabitants[mazeCell])}>
          <div className="ghost-body">
            <div className="ghost-eye">
              <div className="ghost-eye-socket"></div>
            </div>
            <div className="ghost-eye">
              <div className="ghost-eye-socket"></div>
            </div>
            <div className="ghost-skirt">
              <div className="skirt-wave"></div>
              <div className="skirt-wave"></div>
              <div className="skirt-wave"></div>
            </div>
          </div>
        </div>
      </button>
    );
  } else {
    return (
      <button
        className="designed-maze-button"
        onClick={() =>
          dispatch(changeMazeTileAction(state.selectedTileNumber, x, y))
        }
        onAuxClick={() =>
          dispatch(clearMazeTileAction(x, y))
        }
      >
        <div className="path" />
      </button>
    );
  }
};

export default DesignerViewButton;
