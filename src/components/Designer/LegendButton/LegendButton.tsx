import React, { FC } from "react";
import classNames from "classnames";
import LegendButtonShard from "../LegendButtonShard";

import { CNGroup } from "../../../utils/logo";

import Icon from "../../Icons";
import { DesignerTileType } from "../../../utils/enums";
import { designerButtonProps } from "../../../utils/types";

const LegendButton: FC<designerButtonProps> = ({
  tileKey,
  tileNumber,
  tileType
}) => {
  if (
    tileType === DesignerTileType.Wall ||
    tileType === DesignerTileType.Door ||
    tileType === DesignerTileType.Ghome
  ) {
    return (
      <LegendButtonShard
        tileType={tileType}
        tileNumber={tileNumber}
        tileKey={tileKey}
      >
        <Icon className={tileKey} iconToLoad={tileType} />
      </LegendButtonShard>
    );
  } else if (tileType === DesignerTileType.Point) {
    return (
      <LegendButtonShard
        tileType={tileType}
        tileNumber={tileNumber}
        tileKey={tileKey}
        buttonClasses="point-wrapper styled-entity-legend-button"
      >
        <div className="point" />
      </LegendButtonShard>
    );
  } else if (tileType === DesignerTileType.Power) {
    return (
      <LegendButtonShard
        tileType={tileType}
        tileNumber={tileNumber}
        tileKey={tileKey}
        buttonClasses="power-wrapper styled-entity-legend-button"
      >
        <div className="power">
          <img src={CNGroup} alt="CN Group Logo" />
        </div>
      </LegendButtonShard>
    );
  } else if (tileType === DesignerTileType.Pacman) {
    return (
      <LegendButtonShard
        tileType={tileType}
        tileNumber={tileNumber}
        tileKey={tileKey}
        buttonClasses="styled-entity-legend-button"
      >
        <div className="pacman">
          <div className="top-half"></div>
          <div className="bottom-half"></div>
          <div className="eye"></div>
        </div>
      </LegendButtonShard>
    );
  } else if (
    tileType === DesignerTileType.Clyde ||
    tileType === DesignerTileType.Inky ||
    tileType === DesignerTileType.Pinky ||
    tileType === DesignerTileType.Blinky
  ) {
    return (
      <LegendButtonShard
        tileType={tileType}
        tileNumber={tileNumber}
        tileKey={tileKey}
        buttonClasses="ghost-wrapper styled-entity-legend-button"
      >
        <div className={classNames("ghost", tileType)}>
          <div className="ghost-body">
            <div className="ghost-eye">
              <div className="ghost-eye-socket"></div>
            </div>
            <div className="ghost-eye">
              <div className="ghost-eye-socket"></div>
            </div>
            <div className="ghost-skirt">
              <div className="skirt-wave"></div>
              <div className="skirt-wave"></div>
              <div className="skirt-wave"></div>
            </div>
          </div>
        </div>
      </LegendButtonShard>
    );
  } else {
    return (
      <LegendButtonShard
        tileType={tileType}
        tileNumber={tileNumber}
        tileKey={tileKey}
      />
    );
  }
};

export default LegendButton;
