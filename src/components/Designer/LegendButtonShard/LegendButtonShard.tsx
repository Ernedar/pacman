import React, { FC } from "react";
import classNames from "classnames";

import "./LegendButtonShard.css";

import { DesignerTileType } from "../../../utils/enums";
import { designerButtonProps } from "../../../utils/types";
import { useDesignerDispatch, useDesignerState } from "../../../state/mazeDesignerContext";
import { selectTileAction } from "../../../utils/actions";

type legendButtonShardProps = designerButtonProps & {
  buttonClasses?: string;
  children?: React.ReactNode;
};

const LegendButtonShard: FC<legendButtonShardProps> = ({
  tileKey,
  tileNumber,
  tileType,
  buttonClasses,
  children
}) => {
  const state = useDesignerState();
  const dispatch = useDesignerDispatch();
  if (tileType !== DesignerTileType.Path) {
    return (
      <div
        className={classNames("legend-btn-wrapper", {
          selected: tileNumber === state.selectedTileNumber
        })}
      >
        <button
          className={buttonClasses}
          onClick={() =>
            dispatch(selectTileAction(tileNumber, tileKey, tileType))
          }
        >
          {children}
        </button>
        <p>{tileNumber}</p>
      </div>
    );
  } else {
    return (
      <div
        className={classNames("legend-btn-wrapper", {
          selected: tileNumber === state.selectedTileNumber
        })}
      >
        <button
          className="styled-entity-legend-button"
          onClick={() =>
            dispatch(selectTileAction(0, "", DesignerTileType.Path))
          }
        >
          <div className="path" />
        </button>
        <p>path</p>
        <p>empty</p>
      </div>
    );
  }
};

export default LegendButtonShard;
