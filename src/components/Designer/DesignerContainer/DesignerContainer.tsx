import React, { FC, useEffect } from "react";

import "./DesignerContainer.css";

import { refreshStateRenderAction } from "../../../utils/actions";

import DesignerLegend from "../DesignerLegend";
import DesignerView from "../DesignerView";

import { useDesignerState, useDesignerDispatch } from "../../../state/mazeDesignerContext";

const MazeDesigner: FC = () => {
  const state = useDesignerState();
  const dispatch = useDesignerDispatch();

  const lastArrayLineLength =
    state.designedMaze[state.designedMaze.length - 1].length;

  useEffect(() => {
    if (state.designedMaze.length > 1 && lastArrayLineLength === 0) {
      state.designedMaze.pop();
    }
    dispatch(refreshStateRenderAction());
  }, [state.designedMaze, state.designedMaze.length, lastArrayLineLength]);

  return (
    <div className="designer-wrapper">
      <DesignerLegend />
      <DesignerView />
    </div>
  );
};

export default MazeDesigner;
