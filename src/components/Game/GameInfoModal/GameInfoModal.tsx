import React from "react";
import { GameStateType } from "../../../utils/enums";
import classNames from "classnames";
import { changeGameStatus } from "../../../utils/actions";
import {
  useGameState,
  useGameDispatch
} from "../../../state/PacmanGameContext";

import "./GameInfoModal.css";

function GameInfoModal() {
  const state = useGameState();
  const gameStateContextValue = state.game.gameState;
  const dispatch = useGameDispatch();

  const opened =
    gameStateContextValue === GameStateType.Notstarted ||
    gameStateContextValue === GameStateType.Paused ||
    gameStateContextValue === GameStateType.Lost ||
    gameStateContextValue === GameStateType.Finished;

  return (
    <div
      className={classNames("modal-wrapper", {
        opened: opened
      })}
    >
      <div className="modal-card">
        <div className="modal-card-header">
          <h4>PacMan Game</h4>
        </div>
        <div className="modal-card-body">
          <p
            className={classNames({
              "force-hidden": !(
                gameStateContextValue === GameStateType.Notstarted
              )
            })}
          >
            Welcome to PacMan Game. Can you win? Can you eat all points before
            you will be eaten?
          </p>
          <p
            className={classNames({
              "force-hidden": !(gameStateContextValue === GameStateType.Paused)
            })}
          >
            Game Paused. Would you like to resume or start over?
          </p>
          <p
            className={classNames({
              "force-hidden": !(
                gameStateContextValue === GameStateType.Finished
              )
            })}
          >
            Con gratulations. You won. You final score is:
          </p>
          <p
            className={classNames({
              "force-hidden": !(gameStateContextValue === GameStateType.Lost)
            })}
          >
            You got eaten. You final score is:
          </p>
        </div>
        <div className="modal-card-footer">
          <button
            className={classNames("modal-btn", {
              "force-hidden": !(
                gameStateContextValue === GameStateType.Notstarted
              )
            })}
            onClick={() => {
              dispatch(changeGameStatus(GameStateType.Running));
            }}
          >
            Start New Game
          </button>
          <button
            className={classNames("modal-btn", {
              "force-hidden": !(gameStateContextValue === GameStateType.Paused)
            })}
            onClick={() => dispatch(changeGameStatus(GameStateType.Running))}
          >
            Resume Game
          </button>
          <button
            className={classNames("modal-btn", {
              "force-hidden": !(
                gameStateContextValue === GameStateType.Paused ||
                gameStateContextValue === GameStateType.Lost ||
                gameStateContextValue === GameStateType.Finished
              )
            })}
            onClick={() => dispatch(changeGameStatus(GameStateType.Notstarted))}
          >
            Reset Game
          </button>
        </div>
      </div>
    </div>
  );
}

export default GameInfoModal;
