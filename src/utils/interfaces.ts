import {
  PacManStates,
  GhostStates,
  GameStateType,
  DesignerTileType,
  GameActionType,
  DesignerActionType,
  InhabitantNames,
} from './enums';

/* DESIGNER INTERFACES */

export interface designerStateInterface {
  selectedTileType: DesignerTileType;
  selectedTileNumber: number;
  selectedTileClass: string;
  designedMazeID: number;
  designedMazeName: string;
  designedMazeLinkTag: string;
  designedMaze: number[][];
}

/* GAME INTERFACES */

export interface importMaze {
  id: number;
  name: string;
  linktag: string;
  mazeArray: number[][];
}

export interface gameInterface {
  gameState: GameStateType;
  gameScore: number;
  gameLoaded: boolean;
}

export interface movingEntity {
  entityStartPosition: number[];
  entityCurrentPosition: number[];
  entityCurrentDirection: number[];
  entitySpeed: number;
  entityActionCounter: number;
  entityDeltaCounter: number;
}

export interface ghostInterface extends movingEntity {
  entityState: GhostStates;
}

export interface pacmanInterface extends movingEntity {
  entityState: PacManStates;
}

export interface EntitiesInterface {
  pacman: pacmanInterface;
  clyde: ghostInterface;
  inky: ghostInterface;
  pinky: ghostInterface;
  blinky: ghostInterface;
}

export interface gameStateInterface {
  game: gameInterface;
  entity: EntitiesInterface;
  points: { x: number; y: number }[];
  powers: { x: number; y: number }[];
}

/* GAME ACTION INTERFACES */
export interface InitiateGame {
  type: GameActionType.InitiateGame;
  payload: {
    mazeArray: number[][];
  };
}

export interface GameLoaded {
  type: GameActionType.GameLoaded;
}

export interface ChangeGameStatus {
  type: GameActionType.ChangeGameStatus;
  payload: {
    gameStatus: GameStateType;
  };
}

export interface ChangeEntityDirection {
  type: GameActionType.ChangeEntityDirection;
  payload: {
    entity: InhabitantNames;
    direction: number[];
  };
}

export interface UpdateEntityActionCounter {
  type: GameActionType.UpdateEntityActionCounter;
  payload: {
    entity: InhabitantNames;
    entityActionCounter: number;
    entityDeltaCounter: number;
  };
}

export interface UpdateEntityCurrentPosition {
  type: GameActionType.UpdateEntityCurrentPosition;
  payload: {
    newPosition: number[];
    entity: InhabitantNames;
  };
}

/* DESIGNER ACTION INTERFACES */

export interface SelectTileInterface {
  type: DesignerActionType.SelectTileAction;
  payload: {
    tileNumber: number;
    tileClass: string;
    tileType: DesignerTileType;
  };
}

export interface ClearSelectedTileInterface {
  type: DesignerActionType.ClearSelectedTileAction;
}

export interface AddTileToMazeInterface {
  type: DesignerActionType.AddTileToMazeAction;
  payload: {
    tileNumber: number;
    xCoord: number;
  };
}

export interface AddRowOfTilesInterface {
  type: DesignerActionType.AddRowOfTilesAction;
  payload: {
    tileNumber: number;
  };
}

export interface ChangeMazeTileInterface {
  type: DesignerActionType.ChangeMazeTileAction;
  payload: {
    tileNumber: number;
    xCoord: number;
    yCoord: number;
  };
}

export interface ClearMazeTileInterface {
  type: DesignerActionType.ClearMazeTileAction;
  payload: {
    xCoord: number;
    yCoord: number;
  };
}

export interface RemoveTileFromMazeInterface {
  type: DesignerActionType.RemoveTileFromMazeAction;
  payload: {
    xCoord: number;
  };
}

export interface SetMazeNameInterface {
  type: DesignerActionType.SetMazeNameAction;
  payload: {
    mazeName: string;
  };
}

export interface SetMazeLinkTagInterface {
  type: DesignerActionType.SetMazeLinkTagAction;
  payload: {
    mazeLinkTag: string;
  };
}

export interface ClearRowOfTilesInterface {
  type: DesignerActionType.ClearRowOfTilesAction;
  payload: {
    xCoord: number;
  };
}

export interface ClearColOfTilesInterface {
  type: DesignerActionType.ClearColOfTilesAction;
  payload: {
    yCoord: number;
  };
}

export interface SaveMazeInterface {
  type: DesignerActionType.SaveMazeAction;
  payload: {
    designedMaze: number[][];
  };
}

export interface ClearDesignerInterface {
  type: DesignerActionType.ClearDesignerAction;
}

export interface RefreshStateRenderInterface {
  type: DesignerActionType.RefreshStateRenderAction;
}
