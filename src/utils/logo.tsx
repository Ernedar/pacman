import CNGroup from "../assets/cngrouplogo.jpg";
import Angular from "./../assets/technologyIcons/angular-logo.png";
import AWS from "./../assets/technologyIcons/aws-logo.png";
import Azure from "./../assets/technologyIcons/azure-logo.png";
import CSharp from "./../assets/technologyIcons/c-sharp-logo.png";
import Clojure from "./../assets/technologyIcons/clojure-logo.png";
import Drupal8 from "./../assets/technologyIcons/drupal-8-logo.png";
import Elm from "./../assets/technologyIcons/elm-logo.png";
import FSharp from "./../assets/technologyIcons/f-sharp-logo.png";
import Groovy from "./../assets/technologyIcons/groovy-logo.png";
import Java from "./../assets/technologyIcons/java-logo.png";
import Jenkins from "./../assets/technologyIcons/jenkins-logo.png";
import Kotlin from "./../assets/technologyIcons/kotlin-logo.png";
import Kubernetes from "./../assets/technologyIcons/kubernetes-logo.png";
import Moby from "./../assets/technologyIcons/moby-logo.png";
import NodeJS from "./../assets/technologyIcons/nodejs-logo.png";
import PHP from "./../assets/technologyIcons/php-logo.png";
import ReactLogo from "./../assets/technologyIcons/react-logo.png";
import Scala from "./../assets/technologyIcons/scala-logo.png";
import Spring from "./../assets/technologyIcons/spring-logo.png";
import Vue from "./../assets/technologyIcons/vue-logo.png";

export {
  CNGroup,
  Angular,
  AWS,
  Azure,
  CSharp,
  Clojure,
  Drupal8,
  Elm,
  FSharp,
  Groovy,
  Java,
  Jenkins,
  Kotlin,
  Kubernetes,
  Moby,
  NodeJS,
  PHP,
  ReactLogo,
  Scala,
  Spring,
  Vue
};
