import { DesignerTileType } from './enums';
import {
  InitiateGame,
  GameLoaded,
  ChangeGameStatus,
  ChangeEntityDirection,
  UpdateEntityActionCounter,
  UpdateEntityCurrentPosition,
  SelectTileInterface,
  ClearSelectedTileInterface,
  AddTileToMazeInterface,
  AddRowOfTilesInterface,
  ChangeMazeTileInterface,
  ClearMazeTileInterface,
  RemoveTileFromMazeInterface,
  SetMazeNameInterface,
  SetMazeLinkTagInterface,
  ClearRowOfTilesInterface,
  ClearColOfTilesInterface,
  SaveMazeInterface,
  ClearDesignerInterface,
  RefreshStateRenderInterface,
} from './interfaces';

export type designerButtonProps = {
  tileKey: string;
  tileNumber: number;
  tileType: DesignerTileType;
};

export type GameActions =
  | InitiateGame
  | GameLoaded
  | ChangeGameStatus
  | ChangeEntityDirection
  | UpdateEntityActionCounter
  | UpdateEntityCurrentPosition;

export type DesignerActions =
  | SelectTileInterface
  | ClearSelectedTileInterface
  | AddTileToMazeInterface
  | AddRowOfTilesInterface
  | ChangeMazeTileInterface
  | ClearMazeTileInterface
  | RemoveTileFromMazeInterface
  | SetMazeNameInterface
  | SetMazeLinkTagInterface
  | ClearRowOfTilesInterface
  | ClearColOfTilesInterface
  | SaveMazeInterface
  | ClearDesignerInterface
  | RefreshStateRenderInterface;
